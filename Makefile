
all: vccq/pinephone-vccq-mod.dtbo

vccq/pinephone-vccq-mod.dtbo:vccq/pinephone-vccq-mod.dts

%.dtbo : %.dts
	dtc -@ -I dts -O dtb -o $@ $<

install: vccq/pinephone-vccq-mod.dtbo
	install -D vccq/pinephone-vccq-mod.dtbo $(DESTDIR)/boot/dtbo/pinephone-vccq-mod.dtbo
	install -D -m 644 vccq/zz-pinephone-vccq-mod.conf $(DESTDIR)/usr/share/u-boot-menu/conf.d/zz-pinephone-vccq-mod.conf

clean:
	-rm -f vccq/*.dtbo

distclean: clean

uninstall:
	-rm -f $(DESTDIR)/boot/dtbo/pinephone-vccq-mod.dtbo
	-rm -f $(DESTDIR)/usr/share/u-boot-menu/conf.d/zz-pinephone-vccq-mod.conf

.PHONY:all install clean distclean unistall
